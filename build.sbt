name := "API_onlineshop"

version := "0.1"

scalaVersion := "2.11.12"


libraryDependencies ++= Seq(
  "com.typesafe.akka"     %%  "akka-actor"          % "2.5.13",
  "com.typesafe.akka"     %%  "akka-testkit"        % "2.5.13"          % Test,
  "org.scalatest"         %   "scalatest_2.10"      % "2.0"             % "test",
  "com.typesafe.akka"     %%  "akka-actor"          % "2.5.13",

  "io.spray"              %%  "spray-can"           % "1.3.3",
  "io.spray"              %%  "spray-routing"       % "1.3.3",
  "org.json4s"            %%  "json4s-native"       % "3.5.3",
  "org.json4s"            %%  "json4s-jackson"      % "3.5.3",

  "kz.darlab"             %%  "domain-onlineshop"   % "1.1.1",
  "com.rabbitmq"          %   "amqp-client"         % "3.5.3",
  "com.github.sstone"     %   "amqp-client_2.11"    % "1.5",
  "ch.qos.logback"        %   "logback-classic"     % "1.0.3",
  "com.typesafe.akka"     %%  "akka-slf4j"          % "2.5.6",
  "net.debasishg"         %% "redisreact"           % "0.8" % "optional",
  "io.spray"              %% "spray-json"           % "1.3.2" % "optional",
  "org.gnieh" %% "spray-session" % "0.1.0-SNAPSHOT"


)

enablePlugins(JavaAppPackaging)
//libraryDependencies +=  "org.scalatest" % "scalatest_2.10" % "2.0" % "test"
// "org.json4s" %% "json4s-native" % "{latestVersion}",
