import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers
import org.mockito.Mockito._

abstract class Foo {
  def getSomething(): String
}

class WithMockitoSuite extends FunSuite with ShouldMatchers {
  test("mockito") {
    val foo = mock(classOf[Foo])
    when(foo.getSomething()).thenReturn("sss")
    foo.getSomething() should be("sss")
  }
}