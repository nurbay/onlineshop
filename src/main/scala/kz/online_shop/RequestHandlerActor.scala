package kz.online_shop

import java.text.SimpleDateFormat
import java.util.Date

import akka.actor.{Actor, ActorRef, Props}
import kz.darlab.akka.domain.Exceptions.DarErrorCodes.BAD_REQUEST
import kz.darlab.akka.domain.Exceptions.{ApiException, BadRequestException, ErrorInfo}
import kz.darlab.akka.domain.amqp.{DomainMessage, Msgs}
import kz.darlab.akka.domain.entities.DTO_.{DtoProduct, DtoWebUser, Dto_Aggregate}
import kz.darlab.akka.domain.entities._
import kz.online_shop.ampq.RequestPublisherActor.PublishToQueue
import kz.online_shop.util.ApiEndPoint

object RequestHandlerActor{
  def replyTo(apiMessage: String): String   =    s"response.${apiMessage}"
  def props(ref :ActorRef)                  =   Props (new RequestHandlerActor(ref) )

  case class Homepage ()                            extends  ApiAkkaRequest
  case class Login (dto:DtoWebUser)                 extends  ApiAkkaRequest
  case class GetProducts()                          extends  ApiAkkaRequest
  case class GetProductByCategory(i:Int)            extends  ApiAkkaRequest
  case class AddProduct(d:DtoProduct)               extends  ApiAkkaRequest
  case class DeleteProduct (dto:DtoProduct)         extends  ApiAkkaRequest
  case class DeleteProductByID (id:Int)             extends  ApiAkkaRequest
  case class UpdateProduct (dto:DtoProduct)         extends  ApiAkkaRequest
  case class Registr (dto:DtoWebUser)               extends  ApiAkkaRequest
  case class GetAggregate1 (dto:Dto_Aggregate)                         extends  ApiAkkaRequest
  case class GetAggregate2 (st:String)                         extends  ApiAkkaRequest
  case class SendSome (a:Dto_Aggregate)                    extends  ApiAkkaRequest

}
class RequestHandlerActor (publisher:ActorRef) extends  Actor with ApiEndPoint{
  import  RequestHandlerActor._
  val actorName         = self.path.toStringWithoutAddress

  def receive = {
    case Homepage() =>
        val req = DomainMessage.request[String](Msgs.Check, Some(replyTo("checkRequest1")), Map(), "CHECK-MSG" )
        publisher ! PublishToQueue(req , Some(actorName))
        context.become (awaitResponse)

    case GetProducts() =>
        val req =  DomainMessage.request[String](Msgs.GetProduct, Some(replyTo("some")), Map(), "CHECK-MSG" )
        publisher ! PublishToQueue(req , Some(actorName))
        context.become (awaitResponse)

    case AddProduct(d:DTO)  =>
        val req =  DomainMessage.request[DtoProduct](Msgs.AddProduct, Some(replyTo("AddProduct")), Map(), d )
        publisher ! PublishToQueue(req , Some(actorName))
        context.become (awaitResponse)

    case DeleteProduct (dto:DtoProduct) =>
        val req =  DomainMessage.request[DtoProduct](Msgs.DeleteProduct, Some(replyTo("Delete")), Map(), dto)//i.asInstanceOf[java.lang.Integer])
        publisher ! PublishToQueue(req , Some(actorName))
        context.become (awaitResponse)

    case Login (dto:DtoWebUser) =>
        val req =  DomainMessage.request[DtoWebUser](Msgs.UserLogin, Some(replyTo("isLogin")), Map(), dto)//i.asInstanceOf[java.lang.Integer])
        publisher ! PublishToQueue(req , Some(actorName))
        context.become (awaitResponse)

    case Registr (d:DtoWebUser) =>
        val req =  DomainMessage.request[DtoWebUser](Msgs.UserRegistr, Some(replyTo("isLogin")), Map(), d)//i.asInstanceOf[java.lang.Integer])
        publisher ! PublishToQueue(req , Some(actorName))
        context.become (awaitResponse)

    case DeleteProductByID (i:Int) =>
        val req =  DomainMessage.request[Int](Msgs.DeleteProduct, Some(replyTo("deleteById")), Map(), i)
        publisher ! PublishToQueue(req , Some(actorName))
        context.become (awaitResponse)

    case UpdateProduct (dto:DtoProduct) =>
        val req =  DomainMessage.request[DtoProduct](Msgs.UpdateProduct, Some(replyTo("deleteById")), Map(), dto)
        publisher ! PublishToQueue(req , Some(actorName))
        context.become (awaitResponse)

    case GetAggregate2 (s:String) =>
        val req =  DomainMessage.request[String](Msgs.Aggr2, Some(replyTo("GetAgregates2")), Map(), s)
        publisher ! PublishToQueue(req , Some(actorName))
        context.become (awaitResponse)

    case GetAggregate1 (dto:Dto_Aggregate) =>
        val req =  DomainMessage.request[Dto_Aggregate](Msgs.Aggr, Some(replyTo("GetAgregates1")), Map(), dto)
        publisher ! PublishToQueue(req , Some(actorName))
        context.become (awaitResponse)

    case  SendSome (a:Dto_Aggregate) =>
        val req =  DomainMessage.request[Dto_Aggregate](Msgs.PostTest, Some(replyTo("GetAgregates")), Map(), a)
        publisher ! PublishToQueue(req , Some(actorName))
        context.become (awaitResponse)
  }

  def awaitResponse :Receive ={
      case d:  DomainEntity                   =>  context.parent ! d
      case ex: ApiException                   =>  context.parent ! ex
      case ei: ErrorInfo                      =>  context.parent ! ei

  }
}


