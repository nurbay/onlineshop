package kz.online_shop.routing

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.github.sstone.amqp.Amqp.{Ack, Delivery}
import javax.swing.text.DefaultFormatter
import kz.darlab.akka.domain.Exceptions.ErrorInfo
import kz.darlab.akka.domain.amqp.{DomainMessage, DomainObject, Response, SeqEntity}
import kz.darlab.akka.domain.entities.DomainEntity
import kz.online_shop.util.SerializerWithTypeHint
import org.json4s._
import org.json4s.native.JsonMethods._
import org.json4s.jackson.Serialization.read
import org.json4s.native.Serialization
import spray.http.ContentRange.Default
import spray.httpx.Json4sSupport


object ListenerActor {
  def props(publisherActor: ActorRef): Props =  Props(classOf[ListenerActor], publisherActor)
}

class ListenerActor(publisherActor: ActorRef) extends Actor with SerializerWithTypeHint
  with Json4sSupport with ActorLogging {

   def json4sFormats : Formats = formats
  //implicit val frm :Formats = Serialization.formats(NoTypeHints)

  def parseCommand(msgBody: String): DomainMessage[_] = {
    parse(msgBody).extract[DomainMessage[_]]
  }

  override def receive = {
    case d @ Delivery(consumerTag, envelope, properties, body) => {
      val msgBody = new String(body,"utf-8")
     // val msg = read[Response[_]](msgBody)
      log.info("got message: {}",msgBody)

      sender ! Ack(envelope.getDeliveryTag)
      // парсим комманду
      val command = parseCommand(msgBody)

      command match {

        case resp: DomainMessage[_] => {
          val actorName = d.properties.getCorrelationId

          if(actorName != null) {

            val pingEchoActor = context.actorSelection(actorName)

            pingEchoActor ! command.body
          } else {
            log.error("Actor with provided ID was not found")
          }
        }
        case _ =>
          println("-------- case else Listener")
          val actorName = d.properties.getCorrelationId
          val pingEchoActor = context.actorSelection(actorName)
          pingEchoActor ! command.body
      }

    }
  }


}