package kz.online_shop.routing


import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import kz.darlab.akka.domain.Exceptions.{ApiException, ErrorInfo}
import kz.darlab.akka.domain.entities.{DAO_, _}
import kz.darlab.akka.domain.amqp.Accepted
import kz.darlab.akka.domain.entities.DAO_._
import kz.online_shop.RequestHandlerActor.Homepage
import kz.online_shop.routing.PerRequest.WithActorRef
import kz.online_shop.util.SerializerWithTypeHint
import spray.http.{StatusCode, StatusCodes}
import spray.httpx.Json4sSupport
import spray.routing.RequestContext


trait PerRequest  extends  Actor with ActorLogging with Json4sSupport with SerializerWithTypeHint {
  val json4sFormats = formats

  def r: RequestContext
  def target : ActorRef
  def msg: ApiAkkaRequest

  target! msg

  def complete[T <: AnyRef](value: StatusCode, akka: T): Unit = {
    r.complete(value, akka)
    context.stop(self)
  }

  override def receive: Receive ={
    case  ei:ErrorInfo                      =>  complete (StatusCodes.OK,ei)
    case  ae:ApiException                   =>  complete (StatusCodes.OK,ae)
    case  Homepage()                        =>  complete (StatusCodes.OK, DAO_.Homepage)
    case  p: Product                        =>  complete (StatusCodes.OK, p)
    case  ac:Accepted                       =>  complete (StatusCodes.OK, ac )
    case  d:DomainEntity                    =>  complete (StatusCodes.OK,d  )
    case  a                                 =>  complete (StatusCodes.OK, "----------any: "+a)

  }
}

object PerRequest {
  case class WithActorRef(r: RequestContext,msg:ApiAkkaRequest,prop:Props) extends PerRequest{
    def target = context.actorOf(prop)
  }

}

trait PerRequestInit {
  this: Actor =>


  def perRequest (r: RequestContext,msg:ApiAkkaRequest,props:Props): Unit ={
    context.actorOf(Props(new WithActorRef(r,msg,props)))
    println("/// "+r.request.headers)
    println("/// "+r.request.cookies)
    println("context "+r)
  }






}


/*
                    id: Int,
                   category_id: Int,
                   name: String,
                   supplier: Option[String] = None,
                   description: Option[String]

 */