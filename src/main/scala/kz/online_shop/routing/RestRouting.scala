package kz.online_shop.routing

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import javax.swing.text.html.parser.Entity
import kz.darlab.akka.domain.entities.ApiAkkaRequest
import kz.darlab.akka.domain.entities.DAO_.Product
import kz.darlab.akka.domain.entities.DTO_.{DtoProduct, DtoWebUser, Dto_Aggregate}
import kz.online_shop.RequestHandlerActor
import kz.online_shop.util.SerializerWithTypeHint
import spray.httpx.Json4sSupport
import kz.online_shop.RequestHandlerActor._
import spray.routing.session.{InMemorySessionManager, StatefulSessionManager}
import spray.routing.session.directives.StatefulSessionManagerDirectives
import spray.routing.{HttpService, PathMatchers, Route}

import scala.concurrent.duration._

import scala.concurrent.ExecutionContext

class RestRouting (props: Unit=>Props)  extends HttpService     with Actor                          with Json4sSupport
                                                                with SerializerWithTypeHint         with PerRequestInit
                                                                with ActorLogging {
  implicit val executionContext: ExecutionContext = context.dispatcher

  implicit def actorRefFactory = context

  override def receive: Receive = runRoute(route)

  implicit val json4sFormats = formats

  def handleRequest(msg: ApiAkkaRequest): Route = {
    ctx => perRequest(ctx, msg, props())
  }


  val route =
    path("index") {
      handleRequest(Homepage())

    } ~
      path("getproducts") {
        get {
          handleRequest(GetProducts())
        }
      } ~
      path("addproduct") {
        post {
          entity(as[DtoProduct]) { e =>
            handleRequest(AddProduct(e))
          }
        }
      } ~
      path("deleteproduct") {
        post {
          entity(as[DtoProduct]) { e => handleRequest(DeleteProduct(e)) }
        }
      } ~
      path("deleteproduct" / IntNumber) {
        i => {
          get {
            handleRequest(DeleteProductByID(i))
          }
        }

      } ~
      path("login") {
        post {
          entity(as[DtoWebUser]) { dto => handleRequest(Login(dto)) }
        }
      } ~
      path("registr") {
        post {
          entity(as[DtoWebUser]) { wu => handleRequest(Registr(wu))
          }
        }
      } ~
      path("updateproduct") {
        post {
          entity(as[DtoProduct]) { dto => handleRequest(UpdateProduct(dto))
          }
        }
      } ~
      path("agr1") {
        post {
          entity(as[Dto_Aggregate]) { x => handleRequest(GetAggregate1(x)) }
        }
      } ~
      path("agr2") {
        get {
          parameters("name".as[String]) { s => handleRequest(GetAggregate2(s))
          }

        }
      } ~
      path("testPost") {
        post {
          entity(as[Dto_Aggregate]) { x => handleRequest(SendSome(x)) }
        }
      }


}







