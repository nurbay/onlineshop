package kz.online_shop.util

import kz.darlab.akka.domain.amqp.DomainObject

import kz.darlab.akka.domain.serializers.{CoreSerializer, OnlineShopSerializer}
import org.json4s.{FieldSerializer, ShortTypeHints}
import org.json4s.native.Serialization


trait SerializerWithTypeHint extends OnlineShopSerializer with CoreSerializer  {


  implicit val formats = Serialization.formats(
    ShortTypeHints(
      onlineShopTypeHints ++ coreTypeHints
    )
  )
}

