package kz.online_shop.util

import kz.darlab.akka.domain.Exceptions.{DarErrorSeries, DarErrorSystem}

trait ApiEndPoint {

    val errorSystem = DarErrorSystem
    val errorSeries = DarErrorSeries.API


}
