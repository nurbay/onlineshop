package kz.online_shop.ampq


import akka.actor.{ActorRef, ActorSystem}
import com.github.sstone.amqp.Amqp._
import com.github.sstone.amqp.{Amqp, ConnectionOwner, Consumer}
import com.typesafe.config.{ConfigException, ConfigFactory}
import org.slf4j.LoggerFactory

import scala.collection.JavaConverters._
import scala.util.Try

/**
  * AMQP Consumer.
  * This class creates a new AMQP consumer.
  * Service may create consumers on demand.
  * Parameter `configKey` is used to configure consumer
  * Config sample:
  * amqp.consumer {
  *   configKey1 {
  *     queue = "queue.name"             # queue to listen
  *     key = "key.name"                 # key to bind with queue & exchange
  *     passiveQueue = true              # if true, just check that que queue exists
  *     retryExchange = "exchange.name"  # exchange to bind key & queue
  *     exchangeType = "direct"          # exchange type
  *     qos = 0                          # "quality of service", or prefetch count. The number of non-acknowledged messages a channel can receive. If set
  *                                        to one then the consumer using this channel will not receive another message until it has acknowledged or rejected
  *                                        its current message. This feature is commonly used as a load-balancing strategy using multiple consumers and
  *                                        a shared queue.
  *    durableQueue = false              # if true, the queue will be durable i.e. will survive a broker restart
  *    autoDeleteQueue = true            # if true, the queue will be destroyed when it is no longer used
  *   }
  * }
  *
  *
  * @param connection - AMQP connection
  * @param listener - actor that handles received messages
  * @param configKey - configuration key
  */
class AMQPConsumer(connection: AMQPConnection, listener: ActorRef, configKey: String, queueName: Option[String] = None, keyName: Option[String] = None)(implicit context: ActorSystem) {

  val logger = LoggerFactory.getLogger(this.getClass)

  val config = ConfigFactory.load()

  val queue = if(queueName.isDefined) queueName.get else  config.getString(s"amqp.consumer.${configKey}.queue")

  val key = Try{
    if(keyName.isDefined) keyName.get else  config.getString(s"amqp.consumer.${configKey}.key")
  }.toOption

  val keys: Option[Seq[String]] = Try{
    config.getStringList(s"amqp.consumer.${configKey}.keys").asScala
  }.toOption

  val passiveQueue = config.getBoolean(s"amqp.consumer.${configKey}.passiveQueue")
  val passiveExchange = config.getBoolean(s"amqp.consumer.${configKey}.passiveExchange")
  val retryExchange = config.getString(s"amqp.consumer.${configKey}.retryExchange")
  val qos = config.getInt(s"amqp.consumer.${configKey}.qos")
  val exchangeType = config.getString(s"amqp.consumer.${configKey}.exchangeType")
  val durable = try { config.getBoolean(s"amqp.consumer.${configKey}.durableQueue") } catch { case e: ConfigException.Missing => true }
  val autoDelete = try { config.getBoolean(s"amqp.consumer.${configKey}.autoDeleteQueue") } catch { case e: ConfigException.Missing => false }
  val exclusive = try { config.getBoolean(s"amqp.consumer.${configKey}.exclusive") } catch {case e: ConfigException.Missing => false }
  val durableExchange = config.getBoolean(s"amqp.consumer.${configKey}.durableExchange")

  // create a consumer that will route incoming AMQP messages to our listener
  // it starts with an empty list of queues to consume from
  val consumer = ConnectionOwner.createChildActor(connection.conn, Consumer.props(listener, channelParams = None, autoack = false))

  // wait till everyone is actually connected to the broker
  Amqp.waitForConnection(context, consumer).await()

  // create a queue, bind it to a routing key and consume from it
  // here we don't wrap our requests inside a Record message, so they won't replayed when if the connection to
  // the broker is lost: queue and binding will be gone

  // create a queue
  val queueParams = QueueParameters(queue, passive = passiveQueue, durable = durable, exclusive = exclusive, autodelete = autoDelete)

  consumer ! DeclareQueue(queueParams)

  consumer ! DeclareExchange(ExchangeParameters(name = retryExchange, passive = passiveExchange, exchangeType = exchangeType, durable = durableExchange))

  // binding key
  if(key.isDefined) {
    consumer ! QueueBind(queue = queue, exchange = retryExchange, routing_key = key.get)
  }

  // binding keys
  if(keys.isDefined) {

    keys.get.foreach(k => consumer ! QueueBind(queue = queue, exchange = retryExchange, routing_key = k))

  }

  // tell our consumer to consume from it
  consumer ! AddQueue(queueParams)
  logger.debug("queue: "+queue)

}
