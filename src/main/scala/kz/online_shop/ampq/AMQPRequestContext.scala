package kz.online_shop.ampq

case class AMQPRequestContext(correlationId: String, replyTo: String, language: String)