package kz.online_shop.ampq


import akka.actor.{ActorRef, ActorSystem}
import com.github.sstone.amqp.ConnectionOwner
import com.rabbitmq.client.ConnectionFactory
import com.typesafe.config.ConfigFactory

import scala.concurrent.duration._
import scala.util.Try

/**
  * AMQP Connection
  *
  */
trait AMQPConnection {

  /**
    * Connection actor
    */
  val conn: ActorRef

}

/**
  * AMQP connection is created from config files.
  * NOTE: this class requires several config parameters to be defined to conf file:
  * - amqp.connection.host
  * - amqp.connection.post
  * - amqp.connection.user
  * - amqp.connection.password
  * - amqp.connection.reconnectionDelay
  */
class AMQPConnectionConfig(implicit system: ActorSystem) extends AMQPConnection {

  val config = ConfigFactory.load()

  val connFactory = new ConnectionFactory()

  connFactory.setHost(config.getString(s"amqp.connection.host"))
  connFactory.setPort(config.getInt(s"amqp.connection.port"))
  connFactory.setUsername(config.getString(s"amqp.connection.user"))
  connFactory.setPassword(config.getString(s"amqp.connection.password"))

  val virtualHost = Try{config.getString("amqp.connection.virtualHost")}.toOption

  if(virtualHost.isDefined) {
    connFactory.setVirtualHost(virtualHost.get)
  }

  val reconnectionDelay = config.getInt(s"amqp.connection.reconnectionDelay")

  override val conn = system.actorOf(ConnectionOwner.props(connFactory, reconnectionDelay second))

}

/**
  * Simple implementation of the AMQPConnection trait that creates AMQP connection.
  *
  * @param host - host
  * @param port - port
  * @param user - username
  * @param password - password
  * @param reconnectionDelay - reconnection delay
  */
class AMQPConnectionCustom(host: String, port: Int, user: String, password: String, reconnectionDelay: Int, virtualHost: Option[String] = None)(implicit system: ActorSystem) extends AMQPConnection {

  val config = ConfigFactory.load()

  val connFactory = new ConnectionFactory()

  connFactory.setHost(host)
  connFactory.setPort(port)
  connFactory.setUsername(user)
  connFactory.setPassword(password)

  if(virtualHost.isDefined) {
    connFactory.setVirtualHost(virtualHost.get)
  }

  override val conn = system.actorOf(ConnectionOwner.props(connFactory, reconnectionDelay second))
}