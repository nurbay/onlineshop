
import java.text.SimpleDateFormat
import java.util.Date

import org.json4s._

 class  SomeParent()
 object SomeParent

case class ForTesting  (){
  /*
  implicit val formats =  Serialization.formats(NoTypeHints)
    //Serialization.formats(ShortTypeHints(List(classOf[Some])))
  val a = parse(""" { "numbers" : [1, 2, 3, 4] } """)
 val b = parse("""{"name":"Toy","price":35.35}""", useBigDecimalForDouble = true)
println(b.extract[SomeTest])
  println(b)

  val ser = write(SomeTest("pluto",4.5))
  println(ser)
 println ( read[SomeParent](ser) )
*/
  var currentMinuteAsString :Option[String] = None
  try {
    val some = new Date("3/3/3321")
println(some)
    val minuteFormat = new SimpleDateFormat("dd-MM-yyyy")
    currentMinuteAsString = Some(minuteFormat.format(some))
    println(currentMinuteAsString)


  }
  catch {
    case x => println(x)

  }



}
case class SomeTest (name:String,price:Double) extends SomeParent

object ForTesting {
 // val dogSerializer =FieldSerializer[Some](renameTo ("name", "animalname") orElse ignore("owner"), renameFrom("animalname", "name"))
  ForTesting()

  //"jsonClass":"Response","headers":{},
  // "body":{"jsonClass":"Accepted","status":200,"message":"Product successfully added.."},
  // "routingKey":"response.AddProduct"
}
case class FieldSerializer999[A:Manifest](
                                         serializer:   PartialFunction[(String, Any), Option[(String, Any)]] = Map(),
                                         deserializer: PartialFunction[JField, JField] = Map()
                                       )

