import akka.actor.{ActorSystem, Props}
import akka.io.IO
import akka.util.Timeout
import com.github.sstone.amqp.Amqp.Publish
import com.github.sstone.amqp.ConnectionOwner
import com.rabbitmq.client.ConnectionFactory
import com.typesafe.config.ConfigFactory
import kz.online_shop.RequestHandlerActor
import kz.online_shop.ampq.{AMQPConnectionCustom, AMQPConsumer, RequestPublisherActor}
import kz.online_shop.routing.{ListenerActor, RestRouting}
import spray.can.Http

import scala.concurrent.duration._
import scala.util.Try

object Boot extends  App{ //with SimpleRoutingApp {

  implicit val system = ActorSystem("Darlab")

  implicit val timeout = Timeout(3 seconds)

  val config  = ConfigFactory.load()
  val host    = config.getString( "service.host")
  val port    = config.getInt( "service.port")

  def getStringConfigKey(key: String): String = {
    try {
      sys.env(key)
    } catch {
      case e: NoSuchElementException =>
        config.getString(key)
    }
  }

  def getIntConfigKey(key: String): Int = {
    try {
      sys.env(key).toInt
    } catch {
      case e: NoSuchElementException =>
        config.getInt(key)
    }
  }


  val amqpConnection = new AMQPConnectionCustom(
    getStringConfigKey("amqp.connection.host"),
    getIntConfigKey("amqp.connection.port"),
    getStringConfigKey("amqp.connection.user"),
    getStringConfigKey("amqp.connection.password"),
    getIntConfigKey("amqp.connection.reconnectionDelay"),
    Try {
      getStringConfigKey("amqp.connection.virtualHost")
    }.toOption
  )

  val replyExchange = config.getString("amqp.endpoints.exchange")

  val publisherActor = system.actorOf(RequestPublisherActor.props(amqpConnection, replyExchange),name = "amqpPublisher")

  val responseListener = system.actorOf(ListenerActor.props(publisherActor), name = "amqpListener")

  val responseConsumer  = new AMQPConsumer(amqpConnection, responseListener, "reply")

  val reqHandleActor =  (_:Unit ) => Props (new RequestHandlerActor(publisherActor))
  val routeActor = system.actorOf(Props(new RestRouting( reqHandleActor)))

  IO(Http) ! Http.Bind(routeActor, host, port = port)

  import java.security.MessageDigest

  def md5(s: String) = {
    MessageDigest.getInstance("MD5").digest(s.getBytes).map("%02x".format(_)).mkString

  }

  println("Md5 "+ md5("Hello W!#"))

  println(md5("Hello W!#")=="05333cd6926aae3e33e831c41cddf386")



  //def start = startServer(interface = host, port = port) {}
  //-DConfig.resource=local.conf
}


